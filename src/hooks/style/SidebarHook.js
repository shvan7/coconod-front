import { useEffect, useState, useRef } from 'react'

export const useSideBar = (initDisplay) => {
  const refSideBar = useRef(null)
  const [display, setDisplay] = useState(initDisplay)
  const [widthSideBar, setWidthSideBar] = useState(0)

  const update = () => {
    console.log('----------> update')

    setWidthSideBar(
      refSideBar.current ? refSideBar.current.getBoundingClientRect().width : 0,
    )
  }

  useEffect(() => {
    update()
    // window.addEventListener('orientationchange', update)
    // return () => window.removeEventListener('orientationchange', () => update)
  }, [])

  const handleClick = () => {
    setDisplay(!display)
    update()
  }

  // return
  const data = {
    width: widthSideBar,
    ref: refSideBar,
    display: display,
  }

  return [handleClick, { ...data }]
}
