import styled, { css } from 'styled-components'

export const MainBox = styled.div`
  &:hover {
    /* background-color:  */
    filter: invert(100%);
  }

  ${({ styled }) =>
    styled.displaySideBar
      ? css`
          height: calc((100vw - ${styled.marginLeft}px) / 4);
          width: calc((100vw - ${styled.marginLeft}px) / 4);
        `
      : css`
          height: calc(100vw / 4);
          width: calc(100vw / 4);
        `}

  font-family: 'IBM Plex Mono', monospace;
  flex-basis: auto;
  margin: 0;
  padding: 0;
  transition: all 1s ease-in;
  text-align: center;
  background-color: ${(props) => props.color};

  & * {
    color: ${({ hex }) => (Number(hex) <= 9021062 ? 'white' : 'black')};
  }

  @media screen and (max-width: 600px) {
    height: 100%;
    width: 100%;
  }

  & button {
    border: none;
    background-color: transparent;
  }
`
