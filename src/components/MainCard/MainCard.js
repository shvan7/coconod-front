import React from 'react'
import { MainBox } from './MainCard.styled'

const MainCard = ({ data, styled }) => {
  return (
    <MainBox hex={data.hex} color={data.color} styled={{ ...styled }}>
      <div>
        <button>Edit</button>
        <span>|</span>
        <button>Delete</button>
      </div>
      <div>
        <h4>{data.name}</h4>
        <h4>{data.hex}</h4>
        <p>{data.mail}</p>
      </div>
    </MainBox>
  )
}

export default MainCard
