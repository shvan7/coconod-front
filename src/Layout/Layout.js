import React from 'react'
import Navbar from '../containers/Navbar'
import Sidebar from '../containers/Sidebar'
import ContentGrid from '../containers/ContentGrid'
import { LayoutBoddy } from './Layout.styled'
import { useSideBar } from '../hooks/style/SidebarHook'

const Layout = () => {
  const [handleDisplaySideBar, infoSideBar] = useSideBar(true)

  const props = {
    displaySideBar: infoSideBar.display,
    funcDisplay: handleDisplaySideBar,
    widthSideBar: infoSideBar.width,
  }

  return (
    <>
      <Navbar {...props} />
      <LayoutBoddy>
        <Sidebar ref={infoSideBar.ref} {...props} />
        <ContentGrid {...props} />
      </LayoutBoddy>
    </>
  )
}

export default Layout
