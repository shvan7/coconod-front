export const makeUnselectable = (node) => {
  if (node.nodeType === 1) node.setAttribute('unselectable', 'on')

  let child = node.firstChild
  while (child) {
    makeUnselectable(child)
    child = child.nextSibling
  }
}

export const convertHexToDec = (hex) => {}
