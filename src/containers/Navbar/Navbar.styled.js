import styled, { css } from 'styled-components'
import { LockOutlined, UnlockOutlined } from '@ant-design/icons'

export const Nav = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #282f2b;
  height: 2rem;
  width: 100vw;

  /* POLICE */
  text-align: center;
  font-weight: 400;
  letter-spacing: 1px;
  font-size: 1.2rem;

  & > div:first-child {
    ${({ styled }) => {
      return styled.displaySideBar
        ? css`
            transform: translateX(-200%);
          `
        : css``
    }}

    transition: all 0.4s ease-out;
    margin: 0 1em;

    & > h2:hover {
      cursor: pointer;
    }
  }

  & > div {
    display: flex;
    text-align: center;
  }

  & h1,
  h2 {
    margin: auto;
    font-family: 'IBM Plex Sans Condensed', sans-serif;
    text-align: center;
    color: white;
  }
`

export const Img = styled.img`
  margin: 0.2rem;
  width: 1.5rem;
  object-fit: contain;
`

export const BtnIco = styled.button`
  outline: none;
  cursor: pointer;
  color: white;
  border: none;
  background-color: transparent;
  margin: 0 0.5rem;
`

export const LockOutlinedStyled = styled(LockOutlined)`
  font-size: 1rem;

  &:hover {
    color: silver;
  }
`

export const UnlockOutlinedStyled = styled(UnlockOutlined)`
  font-size: 1rem;

  &:hover {
    color: silver;
  }
`
