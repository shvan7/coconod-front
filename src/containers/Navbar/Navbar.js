import React, { useState } from 'react'
import {
  Img,
  Nav,
  BtnIco,
  LockOutlinedStyled,
  UnlockOutlinedStyled,
} from './Navbar.styled'

import logo from '../../assets/image/logo1.png'

const Navbar = React.forwardRef(({ displaySideBar, funcDisplay }, ref) => {
  const [connected, setConnected] = useState(false)

  const handleClick = (event) => {
    setConnected(!connected)
  }

  const styled = {
    displaySideBar,
  }

  return (
    <Nav ref={ref} styled={{ ...styled }}>
      <div>
        <h2 onClick={funcDisplay}>MENU</h2>
      </div>
      <div>
        <Img src={logo} alt="" />
        <h1>COCONOD</h1>
      </div>
      <div>
        <BtnIco onClick={handleClick}>
          {connected ? <LockOutlinedStyled /> : <UnlockOutlinedStyled />}
        </BtnIco>
      </div>
    </Nav>
  )
})

export default Navbar
