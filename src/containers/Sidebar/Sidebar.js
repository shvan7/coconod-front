import React from 'react'
import { Menu } from 'antd'
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
} from '@ant-design/icons'
import { Side, SubMenuStyled, CloseOutlinedStyled } from './Sidebar.styled'

const Sidebar = React.forwardRef(({ funcDisplay, displaySideBar }, ref) => {
  const styled = {
    displaySideBar,
  }

  return (
    <Side ref={ref} styled={{ ...styled }} className="noselect" visible>
      <div>
        <h3>Coucou</h3>
        <button onClick={funcDisplay}>
          <CloseOutlinedStyled />
        </button>
      </div>
      <div>
        <Menu
          onClick={funcDisplay}
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="inline"
        >
          <SubMenuStyled
            key="sub1"
            title={
              <span>
                <MailOutlined />
                <span>Navigation One</span>
              </span>
            }
          >
            <Menu.ItemGroup key="g1" title="Item 1">
              <Menu.Item key="1">Option 1</Menu.Item>
              <Menu.Item key="2">Option 2</Menu.Item>
            </Menu.ItemGroup>
            <Menu.ItemGroup key="g2" title="Item 2">
              <Menu.Item key="3">Option 3</Menu.Item>
              <Menu.Item key="4">Option 4</Menu.Item>
            </Menu.ItemGroup>
          </SubMenuStyled>
          <SubMenuStyled
            key="sub2"
            icon={<AppstoreOutlined />}
            title="Navigation Two"
          >
            <Menu.Item key="5">Option 5</Menu.Item>
            <Menu.Item key="6">Option 6</Menu.Item>
            <SubMenuStyled key="sub3" title="SubMenuStyled">
              <Menu.Item key="7">Option 7</Menu.Item>
              <Menu.Item key="8">Option 8</Menu.Item>
            </SubMenuStyled>
          </SubMenuStyled>
          <SubMenuStyled
            key="sub4"
            title={
              <span>
                <SettingOutlined />
                <span>Navigation Three</span>
              </span>
            }
          >
            <Menu.Item key="9">Option 9</Menu.Item>
            <Menu.Item key="10">Option 10</Menu.Item>
            <Menu.Item key="11">Option 11</Menu.Item>
            <Menu.Item key="12">Option 12</Menu.Item>
          </SubMenuStyled>
        </Menu>
      </div>
    </Side>
  )
})

export default Sidebar
