import styled, { css } from 'styled-components'
import { CloseOutlined } from '@ant-design/icons'
import { Menu } from 'antd'

const { SubMenu } = Menu

export const Side = styled.div`
  /* conditional render sidebar */
  ${({ styled }) => {
    return styled.displaySideBar
      ? css``
      : css`
          transform: translateX(-100%);
        `
  }}

  transition: all 0.4s ease-out;
  position: fixed;
  z-index: 1;
  background-color: white;
  overflow-y: visible;
  height: 100%;
  padding-bottom: 4rem;
  width: 10rem;
  box-shadow: 0 0 1rem 0.1rem #595959;

  /* FIRST CHILD */
  & > div:first-child {
    padding: 0 1rem;
    display: flex;
    position: sticky;
    justify-content: space-between;
    align-items: center;
    z-index: 2;
    right: 0;
    height: 2rem;
    width: 100%;

    & > button {
      width: 3rem;
      height: 3rem;
      border: none;
      background-color: transparent;
    }
  }

  /* SECOND(last) CHILD */
  & > div:last-child {
    background-color: green;
    display: flex;
    text-align: center;
    width: 100%;
  }
`

export const CloseOutlinedStyled = styled(CloseOutlined)`
  &:hover {
    color: black;
  }
`

export const SubMenuStyled = styled(SubMenu)``
