/* eslint-disable */
import React from 'react'
import faker from 'faker'
import { ContentBlock } from './ContentGrid.styled'
import MainCard from '../../components/MainCard'

// F5F9E9
const arrayColor = ['ffff86', '69a742', '5f5f5f']

const sommeString = (number) => {
  const str = number.toString()
  console.log(str)
  const array = str.split('')
  console.log(array)
  return array.reduce((a, b) => Number(a) + Number(b)).toString()
}

const fakeData = () => {
  const data = new Array(6)
  data.fill({})
  return data.map((e) => {
    const randomIndex = Math.floor(Math.random() * arrayColor.length)
    const color = faker.commerce.color()
    //
    return (e = {
      name: faker.name.findName(),
      mail: faker.internet.email(),
      card: faker.helpers.createCard(),
      // color,
      color: '#' + arrayColor[randomIndex],
      hex: parseInt(arrayColor[randomIndex], 16),
    })
  })
}

const ContentGrid = ({ widthSideBar, displaySideBar }) => {
  const styled = {
    marginLeft: widthSideBar,
    displaySideBar,
  }

  const renderBox = () => {
    const data = fakeData()

    return data.map((e, i) => {
      return (
        <MainCard
          key={i + 'mainbox-' + e.name}
          data={e}
          styled={{ ...styled }}
        />
      )
    })
  }

  return <ContentBlock styled={{ ...styled }}>{renderBox()}</ContentBlock>
}

export default ContentGrid
