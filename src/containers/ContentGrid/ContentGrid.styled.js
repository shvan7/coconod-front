import styled from 'styled-components'

export const ContentBlock = styled.div`
  display: flex;
  transition: all 0.4s ease-out;

  flex-flow: row wrap;
  margin-left: ${({ styled }) => (styled.displaySideBar ? '10rem' : 0)};
`
