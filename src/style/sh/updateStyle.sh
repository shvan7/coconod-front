#!/bin/bash

# copy custom less inside antd/dist
# compile it for create new css antd (custom)

echo "-----------------------------"
pwd
echo "-----------------------------"
if cp ./src/style/sh/custom-style.less ./node_modules/antd/dist;
then echo "Copy [OK]"
else echo "Copy [ECHEC]"
fi

if npx lessc -js ./node_modules/antd/dist/custom-style.less ./src/style/custom-antd.css;
then echo "Compile less [OK]"
else echo "Compile less [ECHEC]"
fi
